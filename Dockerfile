# pull the official base image
FROM node:15.14.0-alpine3.10
# set working direction
WORKDIR /app
# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH
# install application dependencies
COPY package.json ./
COPY package-lock.json ./
RUN npm install npm@latest -g
# add app
COPY . ./
# start app
CMD ["npm", "start"]